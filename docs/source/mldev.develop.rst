Developer documentation
=======================

MLDev Base
----------

.. toctree::
   :maxdepth: 4

   codedoc/mldev.experiment
   codedoc/mldev.experiment_tag
   codedoc/mldev.expression
   codedoc/mldev.main
   codedoc/mldev.python_context
   codedoc/mldev.utils
   codedoc/mldev.yaml_loader


MLDev Config Parser
-------------------

.. toctree::
   :maxdepth: 2

   codedoc/mldev_config_parser.config_parser


MLDev Jupyter integration
-------------------------

.. toctree::
   :maxdepth: 4

   codedoc/mldev_jupyter.ipython


MLDev DVC integration
---------------------

.. toctree::
   :maxdepth: 4

   codedoc/mldev_dvc.dvc_stage


MLDev Collab
------------

.. toctree::
   :maxdepth: 4

   codedoc/mldev_collab.collab